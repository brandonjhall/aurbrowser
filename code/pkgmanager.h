#ifndef PKGMANAGER_H
#define PKGMANAGER_H

#include <QObject>
class PkgManagerPrivate;
class PackageVersion;

class PkgManager : public QObject
{
    Q_OBJECT
public:
    explicit PkgManager(QObject *parent = nullptr);
    ~PkgManager();

    bool pkgInstalled(QString pkgName);
    PackageVersion pkgVer(QString pkgName = NULL);
    QStringList listForeignPackages();

signals:
    void alpmError(const QString &message);

public slots:
    void refresh();

private:
    void loadRepos();
    PkgManagerPrivate *p;
};

#endif // PKGMANAGER_H
