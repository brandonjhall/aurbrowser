#ifndef PACKAGEVERSION_H
#define PACKAGEVERSION_H
#include <QVersionNumber>
#include <QDebug>

class PackageVersion : public QVersionNumber
{
public:
    explicit PackageVersion() {}
    PackageVersion(QVersionNumber ver) : QVersionNumber(ver.majorVersion(), ver.minorVersion(), ver.microVersion()) {}

    static PackageVersion fromString(const QString &string)
    {
        int suffixIndex = -1;
        PackageVersion ver(QVersionNumber::fromString(string, &suffixIndex));

        if(suffixIndex != -1)
        {
            int tot = string.length() - suffixIndex - 1;
            QString release = string.right(tot);
            ver.setPkgrel(release.toInt());
        }

        return ver;
    }

    int pkgrel() const {return m_pkgrel;}
    void setPkgrel(int pkgrel) {m_pkgrel = pkgrel;}

    friend bool operator< (const PackageVersion &ver1, const PackageVersion &ver2)
    {
        QVersionNumber num1 = static_cast<QVersionNumber>(ver1);
        QVersionNumber num2 = static_cast<QVersionNumber>(ver2);

        if(num1 == num2)
            return ver1.pkgrel() < ver2.pkgrel();
        else
            return num1 < num2;
    }

    friend bool operator> (const PackageVersion &ver1, const PackageVersion &ver2)
    {
        QVersionNumber num1 = static_cast<QVersionNumber>(ver1);
        QVersionNumber num2 = static_cast<QVersionNumber>(ver2);

        if(num1 == num2)
            return ver1.pkgrel() > ver2.pkgrel();
        else
            return num1 > num2;
    }

    friend bool operator== (const PackageVersion &ver1, const PackageVersion &ver2)
    {
        QVersionNumber num1 = static_cast<QVersionNumber>(ver1);
        QVersionNumber num2 = static_cast<QVersionNumber>(ver2);

        if(num1 == num2)
            return ver1.pkgrel() == ver2.pkgrel();
        else
            return false;
    }

    friend bool operator!= (const PackageVersion &ver1, const PackageVersion &ver2)
    {
        return !(ver1 == ver2);
    }

    friend QDebug operator<< (QDebug stream, const PackageVersion &ver)
    {
        QString versionString = QString("%1.%2.%3-%4").arg(ver.majorVersion()).arg(ver.minorVersion())
                .arg(ver.microVersion()).arg(ver.pkgrel());
        return stream.noquote() << versionString;
    }

private:
    int m_pkgrel;
};

#endif // PACKAGEVERSION_H
