#ifndef ITEMDELEGATES_H
#define ITEMDELEGATES_H
#include <QStyledItemDelegate>

class PopularityDelegate : public QStyledItemDelegate
{
public:
    PopularityDelegate() {}
    QString displayText(const QVariant &value, const QLocale &locale) const {
        if(value.userType() == QVariant::Double)
            return QString::number(value.toDouble(), 'f', 3);
        else
            return QStyledItemDelegate::displayText(value, locale);
    }
};
#endif // ITEMDELEGATES_H
