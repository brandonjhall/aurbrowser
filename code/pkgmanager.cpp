#define ROOTDIR "/"
#define DBPATH "/var/lib/pacman/"

#include "pkgmanager.h"
#include "packageversion.h"
#include <alpm.h>

#include <QTextStream>
#include <QSettings>

class PkgManagerPrivate {
public:
    explicit PkgManagerPrivate(const char *dbpath);
    ~PkgManagerPrivate();

    void getPackage(const char *name);
    bool isPkgForeign(const char *packageName);
    QStringList allPackagesList();
    void refresh();
    void setup();

    alpm_pkg_t *package;
    alpm_handle_t *alpm;
    alpm_errno_t err;
    alpm_db_t *db;
    bool pkgIsInstalled;
    bool isValid;
    const char *currentPkgName;
    const char *rootDir;
    const char *dbpath;
};

PkgManager::PkgManager(QObject *parent) : QObject(parent)
{
    p = new PkgManagerPrivate(DBPATH);
    if(!p->isValid)
        emit alpmError("Failed to initialize alpm");

    loadRepos();
}

PkgManager::~PkgManager()
{
    delete p;
}

bool PkgManager::pkgInstalled(QString pkgName)
{
    if(QString(p->currentPkgName) != pkgName)
        p->getPackage(pkgName.toStdString().c_str());

    return p->pkgIsInstalled;
}

PackageVersion PkgManager::pkgVer(QString pkgName)
{
    if(!pkgName.isNull() && QString(p->currentPkgName) != pkgName)
        p->getPackage(pkgName.toStdString().c_str());

    QString version = alpm_pkg_get_version(p->package);
    return PackageVersion::fromString(version);
}

QStringList PkgManager::listForeignPackages()
{
    QStringList allPkgs = p->allPackagesList();

    if(allPkgs.count() > 0) {
        QStringList aurPkgs;

        foreach (QString packageName, allPkgs) {
            if(p->isPkgForeign(packageName.toStdString().c_str()))
                aurPkgs.append(packageName);
        }

        return aurPkgs;
    }

    return QStringList(NULL);
}

void PkgManager::refresh()
{
    p->refresh();
    if(!p->isValid)
        emit alpmError("Failed to initialize alpm");
}

void PkgManager::loadRepos()
{
    QSettings iniFile("/etc/pacman.conf", QSettings::IniFormat, this);
    QString defaultSigLevel = iniFile.value("options/SigLevel").toString();
    Q_UNUSED(defaultSigLevel)

    foreach (QString child, iniFile.childGroups()) {
        if(child != "options")
            alpm_register_syncdb(p->alpm, child.toStdString().c_str(), ALPM_SIG_USE_DEFAULT);
    }
}

PkgManagerPrivate::PkgManagerPrivate(const char *dbpath)
{
    rootDir = ROOTDIR;
    this->dbpath = dbpath;
    package = nullptr;
    isValid = true;

    setup();
}

PkgManagerPrivate::~PkgManagerPrivate()
{
    if(package) {
        alpm_pkg_free(package);
        package = nullptr;
    }

    alpm_release(alpm);
}

void PkgManagerPrivate::getPackage(const char *name)
{
    if(package) {
        alpm_pkg_free(package);
        package = nullptr;
    }

    package = alpm_db_get_pkg(db, name);

    if(package) {
        pkgIsInstalled = true;
        currentPkgName = alpm_pkg_get_name(package);
    } else {
        pkgIsInstalled = false;
        currentPkgName = (const char *) NULL;
    }
}

bool PkgManagerPrivate::isPkgForeign(const char *packageName)
{
    alpm_list_t *sync_dbs = alpm_get_syncdbs(alpm);
    alpm_list_t *j;

    for(j = sync_dbs; j; j = alpm_list_next(j)) {
        if(alpm_db_get_pkg((alpm_db_t*)j->data, packageName)) {
            return false;
        }
    }

    return true;
}

QStringList PkgManagerPrivate::allPackagesList()
{
    alpm_list_t *allPkgs = alpm_db_get_pkgcache(db);

    if(alpm_list_count(allPkgs) > 0) {
        QStringList pkgsList;
        alpm_list_t *i;

        for(i = allPkgs; i; i = alpm_list_next(i)) {
            alpm_pkg_t *pkg = (alpm_pkg_t*) i->data;

            pkgsList.append(alpm_pkg_get_name(pkg));
        }
        return pkgsList;
    }

    return QStringList(NULL);
}

void PkgManagerPrivate::refresh()
{
    if(package) {
        alpm_pkg_free(package);
        package = nullptr;
    }

    alpm_release(alpm);
    setup();
}

void PkgManagerPrivate::setup()
{
    alpm = alpm_initialize(ROOTDIR, dbpath, &err);

    if(!alpm) {
        QTextStream errOut(stderr);

        errOut << QString("failed to initialize alpm library\n(%1: %2)\n").arg(alpm_strerror(err), dbpath) << endl;

        if(err == ALPM_ERR_DB_VERSION) {
            errOut << "try running pacman-db-upgrade\n" << endl;
        }

        errOut.flush();
        isValid = false;
        return;
    }

    db = alpm_get_localdb(alpm);

    if(!db) {
        QTextStream errOut(stderr);

        errOut << "Failed to open local database\n\n"<< endl;

        errOut.flush();
        isValid = false;
    }
}
