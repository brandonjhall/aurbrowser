#include "networkengine.h"
#include "myoperators.h"

#include <QNetworkCookieJar>
#include <QNetworkDiskCache>
#include <QNetworkReply>
#include <QApplication>
#include <QMessageBox>
#include <QUrlQuery>
#include <QFile>

static const QString aurHost = "aur.archlinux.org";
static const QString aurUrl = "https://" + aurHost;
static const QString aurRpc = aurUrl + "/rpc/";

NetworkEngine::NetworkEngine(QObject *parent) : QNetworkAccessManager(parent)
{
    connectToHostEncrypted(aurHost);
    connectToHostEncrypted(aurHost);
    setCookieJar(new QNetworkCookieJar);
}

void NetworkEngine::findPackage(const QString field, const QString searchText)
{
    QString url = QString(aurRpc + "?v=5&type=search&by=%1&arg=%2").arg(field, searchText);
    sendRPCRequest(QUrl(url));
}

void NetworkEngine::getPackageDetails(const QStringList pkgNames)
{
    QString url = aurRpc + "?v=5&type=info";

    foreach (QString name, pkgNames) {
        url += "&arg[]=" + name;
    }

    sendRPCRequest(QUrl(url));
}

void NetworkEngine::downloadFileToMemory(QUrl url, QString meta)
{
    QNetworkRequest request(url);
    QNetworkReply *reply = get(request);

    connect(reply, &QNetworkReply::finished, this, [reply, meta, this]() {
        if(reply->error())
            return error(reply);

        m_fileData = reply->readAll();
        emit fileDataReady(meta);
        reply->deleteLater();
    });
}

void NetworkEngine::downloadFile(QUrl url)
{
    QNetworkRequest request(url);
    QNetworkReply *reply = get(request);

    connect(reply, &QNetworkReply::finished, this, [reply, url, this]() {
        if(reply->error())
            return error(reply);

        QString fileName = m_tempPath + "/" + url.fileName();
        QFile file(fileName);

        file.open(QFile::ReadWrite);

        if(file.isOpen()) {
            file.write(reply->readAll());
            file.close();
            emit fileSaved(fileName);
        }

        reply->deleteLater();
    });
}

void NetworkEngine::loginToAUR(const QString &name, const QString &password)
{
    QUrl loginUrl = QUrl(aurUrl + "/login/");
    QNetworkRequest login(loginUrl);
    QByteArray paramEncoded;
    QNetworkReply *reply;
    QUrlQuery query;
    QUrl params;

    query.addQueryItem("user", name);
    query.addQueryItem("passwd", password);
    login.setHeader(QNetworkRequest::ContentTypeHeader,"application/x-www-form-urlencoded");
    params.setQuery(query);
    paramEncoded = stringToByteArray(params.query(QUrl::FullyEncoded));

    reply = post(login, paramEncoded);

    connect(reply, &QNetworkReply::finished, this, [reply, this]() {
        if(reply->error())
            return error(reply);

        QString data = reply->readAll();
        if(data.contains("<ul class=\"errorlist\"><li>Bad username or password.</li></ul>")) {
            isLoggedIn = false;
            emit badUsernameOrPassword();
        } else {
            isLoggedIn = true;
            emit loginSuccess();
        }
    });
}

void NetworkEngine::voteForPackage(const QString &pkgbase)
{
    QString token = getToken();
    voteUnvotePackage(pkgbase, "/vote/", token);
}

void NetworkEngine::unvotePackage(const QString &pkgbase)
{
    QString token = getToken();
    voteUnvotePackage(pkgbase, "/unvote/", token);
}

void NetworkEngine::loadPackagePage(const QString &package)
{
    QUrl url(aurUrl + "/packages/" + package);
    QNetworkRequest request(url);
    QNetworkReply *reply = get(request);

    connect(reply, &QNetworkReply::finished, [reply, this]() {
        if(reply->error())
            return error(reply);

        QString line;
        webPage.resize(0);
        if(webPage.open()) {
            do {
                line = reply->readLine();
                webPage.write(stringToByteArray(line));
            } while(!line.isNull());

            webPage.resize(webPage.pos());
            webPage.close();
        }

        if(isLoggedIn)
            checkVote();
    });
}

void NetworkEngine::checkVote()
{
    if(webPage.open()) {
        QString line;
        do {
            line = webPage.readLine();

            if(line.contains("/unvote/")) {
                emit packageIsVoted(true);
                return;
            }

            if(line.contains("/vote/")) {
                emit packageIsVoted(false);
                return;
            }
        } while(!line.isNull());
    }
}

void NetworkEngine::sendRPCRequest(QUrl url)
{
    QNetworkRequest req(url);
    QNetworkReply *reply = get(req);

    connect(reply, &QNetworkReply::finished, [reply, this]() {
        if(reply->error())
            return error(reply);

        m_data = reply->readAll();

        if(!m_data.isEmpty())
            emit dataReady();

        reply->deleteLater();
    });
}

void NetworkEngine::error(QNetworkReply *reply)
{
    QApplication::restoreOverrideCursor();
    QMessageBox mbox(QApplication::activeWindow());

    mbox.setText(reply->errorString());
    mbox.setWindowTitle("Network Error");
    mbox.setIcon(QMessageBox::Warning);
    mbox.exec();
    reply->deleteLater();
}

bool NetworkEngine::getRememberMe() const
{
    return m_rememberMe;
}

void NetworkEngine::setRememberMe(bool rememberMe)
{
    m_rememberMe = rememberMe;
}

void NetworkEngine::setPackage(const QString &package)
{
    m_packageName = package;
    loadPackagePage(package);
}

void NetworkEngine::voteUnvotePackage(const QString &pkgbase, const QString &votePath, const QString &token)
{
    QUrl url = QUrl(aurUrl + "/pkgbase/" + pkgbase + votePath);
    bool isVote = (votePath == "/vote/");
    QNetworkRequest request;
    QNetworkReply *reply;
    QByteArray queryData;
    QUrlQuery query;
    QUrl params;

    query.addQueryItem("token", token);
    query.addQueryItem((isVote ? "do_Vote" : "do_UnVote"), "(Un)Vote");
    params.setQuery(query);
    queryData = stringToByteArray(params.query(QUrl::FullyEncoded));
    request = QNetworkRequest(url);

    request.setHeader(QNetworkRequest::ContentTypeHeader, "application/x-www-form-urlencoded");
    reply = post(request, queryData);

    connect(reply, &QNetworkReply::finished, this, [reply, isVote, this]() {
        if(reply->error())
            return error(reply);

        loadPackagePage(m_packageName);
        emit voteChanged(isVote);
    });
}

QString NetworkEngine::getToken()
{
    if(webPage.open()) {
        QString packageHtml = webPage.readAll();
        QRegExp rx("<input type=\"hidden\" name=\"token\" value=\"[a-fA-F0-9]{32}\"");
        QRegExp value("[a-fA-F0-9]{32}");
        int index = rx.indexIn(packageHtml);
        QStringList matches = rx.capturedTexts();
        index = value.indexIn(matches.first());
        Q_UNUSED(index)

    return value.capturedTexts().first();
    }

    return QString();
}

QString NetworkEngine::getFileData() const
{
    return m_fileData;
}

void NetworkEngine::setTempPath(const QString &tempPath)
{
    QNetworkDiskCache *diskCache = new QNetworkDiskCache(this);
    m_tempPath = tempPath;

    diskCache->setCacheDirectory(m_tempPath + "/cache/");
    setCache(diskCache);
}

QByteArray NetworkEngine::getData()
{
    QByteArray arr = m_data;
    m_data.clear();
    return arr;
}
