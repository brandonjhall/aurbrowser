#ifndef NETWORKENGINE_H
#define NETWORKENGINE_H
#include <QNetworkAccessManager>
#include <QTemporaryFile>

class NetworkEngine : public QNetworkAccessManager
{
    Q_OBJECT
public:
    explicit NetworkEngine(QObject *parent = nullptr);
    void findPackage(const QString field, const QString searchText);
    void getPackageDetails(const QStringList pkgNames);
    void downloadFileToMemory(QUrl url, QString meta = NULL);
    void downloadFile(QUrl url);
    void loginToAUR(const QString &name, const QString &password);
    void voteForPackage(const QString &pkgbase);
    void unvotePackage(const QString &pkgbase);
    void loadPackagePage(const QString &package);
    void checkVote();

    void setTempPath(const QString &tempPath);
    QString getFileData() const;
    QByteArray getData();

    bool getRememberMe() const;
    void setRememberMe(bool rememberMe);

public slots:
    void setPackage(const QString &package);

signals:
    void badUsernameOrPassword();
    void fileSaved(QString fileName);
    void fileDataReady(QString metaData);
    void packageIsVoted(bool isVoted);
    void voteChanged(bool vote);
    void loginSuccess();
    void dataReady();

private:
    void voteUnvotePackage(const QString &pkgbase, const QString &votePath, const QString &token);
    QString getToken();
    void sendRPCRequest(QUrl url);
    void error(QNetworkReply *reply);

    QTemporaryFile webPage;
    QString m_packageName;
    QString m_tempPath;
    QString m_fileData;
    QByteArray m_data;
    bool m_rememberMe;
    bool isLoggedIn;
};
#endif // NETWORKENGINE_H
