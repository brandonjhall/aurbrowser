/***************************************************************************
 *   Copyright 2018 by Brandon Hall                                        *
 *   brandonjhall@windstream.net                                           *
 *                                                                         *
 *   This file is part of AUR Browser                                      *
 *   AUR Browser is free software; you can redistribute it and/or modify   *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   AUR Browser is distributed in the hope that it will be useful,        *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with AUR Browser. If not, see <http://www.gnu.org/licenses/>.   *
 **************************************************************************/

#include <QTemporaryFile>
#include <QApplication>
#include <QProcess>
#include <QDebug>
#include <QDir>

#include "commandprocessor.h"

CommandProcessor::CommandProcessor(QObject *parent) : QObject(parent)
{
    QFile tempScript(QTemporaryFile::createNativeFile(":/install_pkg.sh")->fileName());

    tempScript.rename(tempScript.fileName() + ".sh");
    script = tempScript.fileName();
}

CommandProcessor::~CommandProcessor()
{
    QFile(script).remove();
}

void CommandProcessor::makepkg(const QString &package, const QString &archive)
{
    if(isRunning) {
        QPair<QString,QString> item;

        item.first = package;
        item.second = archive;
        que.append(item);
        return;
    }

    currentPackage = package;
    runScript(archive);
    isRunning = true;
}

void CommandProcessor::extractTar(QString file)
{
    currentProcess = setupProcess();

    currentProcess->setProgram("tar");
    currentProcess->setArguments(QStringList() << "-xzf" << file);
    currentProcess->start();
}

QProcess *CommandProcessor::setupProcess()
{
    QProcess *proc = new QProcess(this);

    proc->setProcessChannelMode(QProcess::MergedChannels);

    connect(proc, &QProcess::readyReadStandardOutput, this, [proc, this]() {
        proc->setReadChannel(QProcess::StandardOutput);
        processOutput();
    });

    connect(proc, &QProcess::readyReadStandardError, this, [proc, this]() {
        proc->setReadChannel(QProcess::StandardError);
        processOutput();
    });

    connect(proc, &QProcess::errorOccurred, this, [this](QProcess::ProcessError error) {
        Q_UNUSED(error)
    });

    connect(proc, QOverload<int, QProcess::ExitStatus>::of(&QProcess::finished), this,
            [proc, this](int exitCode, QProcess::ExitStatus exitStatus) {
        if(exitStatus == QProcess::NormalExit) {
            if(proc->program() == "bash") {
                emit finished(currentPackage);
                isRunning = false;
            }
        } else {
            isRunning = false;
        }

        m_output.append(QString("Finished with an exit code of %1\n").arg(exitCode));
        emit outputReady();
        queNext();
    });

    return proc;
}

void CommandProcessor::processOutput()
{
    while(currentProcess->canReadLine()) {
        QString line = currentProcess->readLine();

        if(line.contains('\r')) {
            QStringList lines = m_output.split('\n');

            lines.removeLast();
            m_output = lines.join('\n') + "\n";
        }

        m_output.append(line);
    }

    emit outputReady();
    QApplication::processEvents();

    if(!currentProcess->atEnd()) {
        QString remain = currentProcess->readAll();
        QChar defaultChoice;

        m_output.append(remain);
        if(remain.contains("[y/n]", Qt::CaseInsensitive)) {
            if(remain.contains("[Y/"))
                defaultChoice = 'Y';
            else
                defaultChoice = 'N';

            emit outputReady();
            emit ynPrompt(remain, defaultChoice);
        }
    }
}

void CommandProcessor::runScript(QString file)
{
    QString dir = QString(file).remove(".tar.gz");
    currentProcess = setupProcess();
    currentProcess->setProgram("bash");
    currentProcess->setArguments(QStringList() << script << file << dir << "-sic");
    currentProcess->start();
}

QString CommandProcessor::output() const
{
    return m_output;
}

void CommandProcessor::decline()
{
    currentProcess->write("n\n");
}

void CommandProcessor::accept()
{
    currentProcess->write("y\n");
}

void CommandProcessor::queNext()
{
    if(que.count() > 0) {
        QPair<QString,QString> item = que.takeFirst();

        runScript(item.second);
        isRunning = true;
    }
}
