#ifndef MYOPERATORS_H
#define MYOPERATORS_H
#include <QString>

inline QByteArray stringToByteArray(const QString &string)
{
    return string.toStdString().c_str();
}
#endif // MYOPERATORS_H
