/***************************************************************************
 *   Copyright 2018 by Brandon Hall                                        *
 *   brandonjhall@windstream.net                                           *
 *                                                                         *
 *   This file is part of AUR Browser                                      *
 *   AUR Browser is free software; you can redistribute it and/or modify   *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   AUR Browser is distributed in the hope that it will be useful,        *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with AUR Browser. If not, see <http://www.gnu.org/licenses/>.   *
 **************************************************************************/

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "packageversion.h"

class PkgManager;
class MainWindowPrivate;
class CommandProcessor;
class QStringListModel;
class QItemSelection;
class NetworkEngine;
class JsonParser;

namespace Ui {
class MainWindow;
class Dialog;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

public slots:
    void checkForUpdates();
    void installProgram();
    void onMoreDetails();
    void onSearch();
    void about();

private:
    int findSortColumn(QString text = QString());
    void closeEvent(QCloseEvent *);
    void saveSettings();
    void initialize();
    bool downloadPkg();
    void processUpdates();

    QStringListModel *completerList;
    JsonParser *parser = nullptr;
    QStringList downloadableURLs;
    CommandProcessor *installer;
    NetworkEngine *netEngine;
    QString selectedPackage;
    QString selectedPkgBase;
    PackageVersion selectedPkgVer;
    PkgManager *pkgManager;

    MainWindowPrivate *p;
    QString currentFile;
    Ui::MainWindow *ui;
    QByteArray dialogState;
    bool detailsWasEnabled;
    bool dockWasVisible;
    bool checkingUpdates;
    Ui::Dialog *loginUi;
    QDialog *loginDialog;
    bool canVote;

private slots:
    void onYnPrompt(const QString &prompt, const QChar &defaultChoice);
    void onTableClicked(const QItemSelection &selection);
    void onModelReady();
    void viewCurrentPKGBUILD();
    void viewInAUR();
    void onFileDataReady(QString metaData);
    void onDetailsReady();
    void onError();
    void onTabChange(int index);
    void onVote(bool vote);
    void onDownloadOnly();
    void onLogin();
};

#endif // MAINWINDOW_H
