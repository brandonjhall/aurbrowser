#/bin/bash
TAR_FILE=$1
PKG_DIR=$2
MAKEPKG_ARGS=$3

tar -xzf $TAR_FILE
cd $PKG_DIR
makepkg $MAKEPKG_ARGS
